//Log text to the debugger
function log(text) {
	console.log(text);
}

//Used for simple debug messages
function debug(text) {
	console.log("[DEBUG] " + text);
}

//Gives the coords of an object
function coords(object) {
	console.log("[COORDS] " + object.x + ", " + object.y);
}

//Logs an error
function error(text) {
	console.log("[ERROR] " + text);
}

//Converts a number from int to string (i.e: 1000 to 1K, 15000 to 15k, 10500 to 10.5K)
function intToText(number) {
    if(number < 10000 && number >= 1000) {
        var numberString = number.toString();
        var text = numberString.substring(-4, 3);
        var text2 = text.slice(0, 1) + '.' + text.slice(1) + 'K';
        return text2;
    }else if(number < 100000 && number >= 10000) {
        var numberString = number.toString();
        var text = numberString.substring(-4, 3);
        var text2 = text.slice(0, 2) + '.' + text.slice(2) + 'K';
        return text2;
    }else if(number < 1000000 && number >= 100000) {
        var numberString = number.toString();
        var text = numberString.substring(-4, 4);
        var text2 = text.slice(0, 3) + '.' + text.slice(3) + 'K';
        return text2;
    }else if(number < 1000000000 && number >= 1000000) {
        console.log("This is a really big number. You shouldn't use this for converting it.");
    }else{
        return number;   
    }
}

function toggleBool(bool) {
	if(bool === false) {
		bool = true;
	}else if(bool === true) {
		bool = false;
	}
}

function minutesToHours(minutes) {
	var hours, minutes2 = 0;
	
	var hours = Math.floor(minutes / 60);
	
	var minutes2 = minutes % 60;
	
	if(hours < 10 && minutes2 < 10) {
		return "0" + hours + ":0" + minutes2;
	}else if(hours < 10 && minutes2 >= 10) {
		return "0" + hours + ":" + minutes2;
	}else if(hours > 10 && minutes2 <= 10) {
		return hours + ":0" + minutes2;
	}else{
		return hours + ":" + minutes2;
	}
}