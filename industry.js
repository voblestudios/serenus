function Blueprint(input, output, craftingTime) {
	this.input = input;
	this.output = output;
	this.craftingTime = craftingTime;
};

function IndustryTask(blueprint, station) {
	this.blueprint = blueprint;
	this.station = station;
};

var blueprintTest = new Blueprint([[silicon, 5], [aluminium, 5]], magnesium, 10);
var craftingTest = new Blueprint([[aluminium, 75],[uranium, 2]], platinum, 20);

blueprintTest.name = "blueprintTest";
craftingTest.name = "craftingTest";

var blueprints = [blueprintTest, craftingTest];

var industryTasksInProgress = [];