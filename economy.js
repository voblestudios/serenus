//Declares the market and the resources contained in it
var market = {
	name:"Planetary Market",
	level:1,
	rating:1,
	Magnesium:0,
	Silicon:0,
	Aluminium:0,
	Chromium:0,
	Iron:0,
	Zinc:0,
	Copper:0,
	Titanium:0,
	Platinum:0,
	Uranium:0
};

//Upgrade the market to the next tier
function upgradeMarket() {
	if(market.level === 1) {
		market.name = "Stellar Market";
		market.level += 1;
	}else if(market.level === 2) {
		market.name = "Galactic Market";
		market.level += 1;
	}else if(market.level === 3) {
		market.name = "Universal Market";
		market.level += 1;
	}else{
		console.log("The market is already upgraded.");
	}
}


//Add a certain amount of a resource to the market
function addToMarket(resource, amount) {
	market[resource] += amount;
}

//Remove a certain amount of a resource from the market
function removeFromMarket(resource, amount) {
	market[resource] -= amount;
}

//Calculates all the prices using the calculatePrice function
function calculatePrices() {
	magnesium.value = calculatePrice(magnesium);
	silicon.value = calculatePrice(silicon);
	aluminium.value = calculatePrice(aluminium);
	chromium.value = calculatePrice(chromium);
	iron.value = calculatePrice(iron);
	zinc.value = calculatePrice(zinc);
	copper.value = calculatePrice(copper);
	titanium.value = calculatePrice(titanium);
	platinum.value = calculatePrice(platinum);
	uranium.value = calculatePrice(uranium);
}

//Calculate all the demands using the calculateDemand function
function calculateDemands() {
	magnesium.demand = calculateDemand(magnesium);
	silicon.demand = calculateDemand(silicon);
	aluminium.demand = calculateDemand(aluminium);
	chromium.demand = calculateDemand(chromium);
	iron.demand = calculateDemand(iron);
	zinc.demand = calculateDemand(zinc);
	copper.demand = calculateDemand(copper);
	titanium.demand = calculateDemand(titanium);
	platinum.demand = calculateDemand(platinum);
	uranium.demand = calculateDemand(uranium);
}

//Calculates the price of a resource
function calculatePrice(resource) {
	return resource.base +  (resource.demand) / 4;
}

//Calculates the demand of a resource
function calculateDemand(resource) {
	return market[resource.name] / 100;
}

//Buys a certain amount of a resource
function buy(amount, resource, cost) {
	
}

//Sells a certain amount of a resoruce
function sell(amount, resource, cost) {
	
}