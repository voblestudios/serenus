//Declare the mission
function Mission(id, target, completed, reward) {
	this.id = id;
	this.target = target;
	this.completed = completed;
	this.reward = reward;
	index++;
}

function giveMission(mission) {
	missions.push(mission.id);
}

function completeMission(mission) {
	console.log("Mission completed!");
	mission.completed = true;
	credits += mission.reward;
}