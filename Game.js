			var game = new Phaser.Game(innerWidth, innerHeight, Phaser.AUTO, 'serenus', { preload: preload, create: create, update: update, render: render });
			
			var motorState = 1;
			
			var docking = false;
			var mining = false;

			var DRAG = 100;
			
			var mission1;
			var mission2;
			
			var mining2 = 0;
			
			var marketOpen = false;
			var cargoOpen = false;
			var processingOpen = false;
			
			var centerX, centerY;
			
			var minerals2 = minerals.reverse();
			
			var miningLaser;
			
			//Loads the assets for the game
			function preload() {
				game.load.image('ship', 'assets/BasicShip.png');
				game.load.image('asteroid', 'assets/asteroid.png');
				game.load.image('star', 'assets/star.png');
				game.load.image('planet1', 'assets/planet1.png');
			}
			
			//Generic function to create a sprite
			function createSprite(texture, x, y, physics) {
				var sprite = game.add.sprite(x, y, texture);
				if(physics === true) {
					game.physics.arcade.enable(sprite);
					sprite.body.drag.setTo(DRAG, DRAG);
					sprite.body.gravity.y = 0;
				}
				sprite.smoothed = true;
				sprite.anchor.setTo(0.5);
				return sprite;
			}

			function create() {
				log($("#BlueprintGeneralInfoAmountField").value);
				
				if(typeof(Worker) !== "undefined") {
					log("Worker is good to go!");
				}
				
				//Set the bounds of the world
				game.world.setBounds(0,0,10000000,10000000);
				
				//Creates keyboard input
				var marketKey = game.input.keyboard.addKey(Phaser.Keyboard.M);
				var holdKey = game.input.keyboard.addKey(Phaser.Keyboard.C);
				var miningKey = game.input.keyboard.addKey(Phaser.Keyboard.G);
				var processingKey = game.input.keyboard.addKey(Phaser.Keyboard.P);
				
				marketKey.onDown.add(function(marketKey)
				{
					if(marketOpen === false) {
						document.getElementById("Market").style.visibility = 'visible';
						marketOpen = true;
						game.paused = true;
					}else{
						document.getElementById("Market").style.visibility = 'hidden';
						marketOpen = false;
						game.paused = false;
					}
				}, this);
				
				holdKey.onDown.add(function(holdKey)
				{
					if(cargoOpen === false) {
						updateHold();
						document.getElementById("CargoHold").style.visibility = 'visible';
						cargoOpen = true;
						game.paused = true;
					}else{
						document.getElementById("CargoHold").style.visibility = 'hidden';
						cargoOpen = false;
						game.paused = false;
					}
				}, this);
				
				processingKey.onDown.add(function(processingKey)
				{
					if(processingOpen === false) {
						updateBlueprintList();
						document.getElementById("ResourceProcessing").style.visibility = 'visible';
						processingOpen = true;
					}else{
						document.getElementById("ResourceProcessing").style.visibility = 'hidden';
						processingOpen = false;
					}
				}, this);
				
				miningKey.onDown.add(function(miningKey)
				{
					if(mining === false) {
						mining = true;
					}else{
						mining = false;
					}
				}, this);
				
				$("#Label").textfill({
					
				});
				
				$("#CategoriesTag").textfill({
					
				});
				
				$(".ResourceCategory").textfill({
					
				});
				
				$("#HoldLabel").textfill({});
				
				centerX = game.world.width / 2;
				centerY = game.world.height / 2;
				
				console.log("Welcome to Serenus, a game by Voble Studios!");
				
				this.game.time.advancedTiming = true;
				//Enabling physics
				game.physics.startSystem(Phaser.Physics.ARCADE);

				//Creating the player
				player = game.add.sprite(centerX, centerY, 'ship');
				game.physics.arcade.enable(player);
				player.body.gravity.y = 0;
				player.smoothed = true;
				player.anchor.setTo(0.5, 0.5);
				player.scale.setTo(0.5, 0.5);
				player.body.drag.setTo(DRAG, DRAG);
				player.body.setSize(140, 150, 0, 0);
				
				//Crosshair
				crosshair = createSprite('asteroid', game.input.x, game.input.y, false);
				
				//Adding the asteroid for testing
				asteroid = createSprite('asteroid', centerX + 100, centerY + 100, true);
				
				asteroid2 = createSprite('asteroid', centerX + 500, centerY + 500, true);
				
				star = createSprite('star', centerX, game.world.height / 2, false);
				
				generateUniverse();
				
				//Adding groups
				asteroids = game.add.group();
				ships = game.add.group();
				
				//Adding sprites to groups
				ships.add(player);
				asteroids.add(asteroid);
				asteroids.add(asteroid2);

				game.physics.arcade.enable(player);
				this.camera.follow(player);
				
				//Create test missions
				mission1 = new Mission(index, asteroid, false, 1000);
				giveMission(mission1);
				
				mission2 = new Mission(index, asteroid2, false, 5000);
				giveMission(mission2);
				
				miningLaser = new Phaser.Line(player.x, player.y, game.input.x, game.input.y);
			}

			function update() {
				var distance = Math.sqrt(Math.pow(innerWidth / 2 - game.input.x, 2) + Math.pow(innerHeight / 2 - game.input.y, 2));
				miningLaser.fromAngle(player.x, player.y, game.physics.arcade.angleToPointer(player), distance);
				
				calculateDemands();
				calculatePrices();
				
				//Sets the rotation of the player to the pointer
				player.rotation = game.physics.arcade.angleToPointer(player);

				//Movement
				if(game.input.keyboard.isDown(Phaser.Keyboard.W)) {
					  game.physics.arcade.velocityFromAngle(player.angle, 200 + (100 * motorState), player.body.velocity);
				}else if(game.input.keyboard.isDown(Phaser.Keyboard.S)) {
					  game.physics.arcade.velocityFromAngle(player.angle, -50, player.body.velocity);
				}else if(game.input.keyboard.downDuration(Phaser.Keyboard.T)) {
					player.x = 100000;
					player.y = -50000;
				}else if(game.input.keyboard.downDuration(Phaser.Keyboard.D)) {
					if(!docking) {
						docking = true;
					}else{
						docking = false;
					}
				}else if(game.input.keyboard.downDuration(Phaser.Keyboard.U)) {
					completeTransaction();
				}
				
				//Check if mission is completed
				if(missions.length > 0) {
					for(var i = 0, j = missions.length; i < j; i++) {
						var missionN = "mission" + (i + 1);
						if(game.physics.arcade.overlap(player, eval(missionN).target) && eval(missionN).completed === false) {
							completeMission(eval(missionN));
						}
					}
				}
				
				$("div#Credits").html(function() {
					var value = 'Credits : ' + credits;
					return value;
				});
				
				$("div#Science").html(function() {
					var value = "Science : " + science;
					return value;
				});
				
				if(mining) {
					if(mining2 >= 1000) {
						mine();
						mining2 -= 1000;
					}
					
					asteroids.forEach(function(thisAsteroid) {
						var coordsOnLine = [];
						for(var i = 0, j = miningLaser.coordinatesOnLine().length; i < j; i++) {
							miningLaser.coordinatesOnLine(5, coordsOnLine);
							if(coordsOnLine[i][0] <= thisAsteroid.body.halfWidth + thisAsteroid.x && coordsOnLine[i][1] <= thisAsteroid.body.halfHeight + thisAsteroid.y && coordsOnLine[i][0] >= thisAsteroid.x - thisAsteroid.body.halfWidth && coordsOnLine[i][1] >= thisAsteroid.y - thisAsteroid.body.halfHeight) {
								mining2++;
							}
						}
					});	
				}
			}

			function render() {
				game.debug.text("Serenus ALPHA", innerWidth - 200, 64, 'rgb(255,255,255)');
				if(mining !== false) {
					game.debug.geom(miningLaser);
				}
				game.debug.text("FPS:" + this.game.time.fps, 10, 64, 'rgb(255,255,255)');
			}
			
			function updateCostLabel(r) {
				if(parseInt(r.value) >= 0) {
					$(r).parent().parent().children('div.ResourceCost').html(function() {
						var value = intToText(0 - eval($(r).parent().parent().attr('title').toLowerCase()).value * r.value) + " credits";
						return value;
					});
				}else{
					$(r).parent().parent().children('div.ResourceCost').html(function() {
						var value = intToText(0 - eval($(r).parent().parent().attr('title').toLowerCase()).value * (r.value / 2)) + " credits";
						return value;
					});
				}
			}
			
			function getMarketAmount(resource) {
				return market[resource];
			}
			
			//Returns the elements contained in a category
			function getCategory() {
				
				//Removes all of the HTML from the resource list
				$("div#ResourceList").html(function() {
					return null;
				});
				
				//Adds in a div to go back to category selection
				$("div#ResourceList").html(function() {
					var content = '<div id="ResourcesList" style="display: table; position: relative; width: 100%; height: 10%; background: red; border-bottom: 1px white solid;" onclick="resetMarket(this);">	<span class="CategoryName" title="Minerals">Categories</span> </div>';
					return content;
				});
				$("#ResourcesList").textfill({});
				
				//
				for(i = 0, j = minerals.length; i < j; i++) {
					$("div#ResourcesList").after(' <div class="ResourcePreview" style="background-color: aqua; width: 100%; height: 10%; display: table; border-bottom: white 1px solid;" title="' + minerals2[i].name + '" onclick="addResourceToCart(this);">	<span class="ResourcePreviewName" style="display: table-cell; text-align: left; vertical-align: middle;">' + minerals2[i].name + '</span>  <span class="ResourcePreviewCost" style="display: table-cell; text-align: right; vertical-align: middle;">Cost:' + intToText(minerals2[i].value) +'C/t</span> </div>');
				}
			}
			
			function updateHold() {
				$("#CargoHold").empty();
				$("#CargoHold").html('<div id="HoldLabel"><span style="text-align: center; vertical-align: middle;">Cargo Hold</span></div>');
				$("#HoldLabel").textfill({});
				
				for(var i = 0, j = minerals2.length; i < j; i++) {
					$("#HoldLabel").after(function() {
						log(minerals2[i]);
						return '<div class="CargoResource"><span style="text-align: center; vertical-align: middle; position: absolute; left: 0; top: 25%;">' + minerals2[i].name + '</span><span style="text-align: center; vertical-align: middle; position: absolute; right: 0; top: 25%; margin-right: 3%;">' + intToText(cargo[minerals2[i].name]) + '</span></div>';
					});
				}
			}
			
			//Adds a resource to the shopping cart
			function addResourceToCart(r) {
				var alreadyInCart = false;
				
				$("#Resources").children().each(function(){
					if(this.getAttribute("title") === r.getAttribute("title") && alreadyInCart === false) {
						alreadyInCart = true;
					}
				});
				
				if(!alreadyInCart) {
					$("div#MarketMenu").after(function() {
						return '<div class="Resource" oncontextmenu="removeResourceFromCart(this);" title="'+ r.getAttribute("title") +'"><div class="ResourceName"">' + r.getAttribute("title") + '</div><div class="ResourceTrade"><input type="number" id="ResourceAmount" max="100000" onkeypress="return event.charCode >=48 && event.charCode <= 57" onChange="updateCostLabel(this);" onInput="updateCostLabel(this);" onkeyup="updateCostLabel(this);" value="0" placeholder="0"></div><div class="ResourceCost">0 Credits</div></div>';
				});
					$(r).remove();
				}
			}
			
			//Removes a resource from the shopping cart
			function removeResourceFromCart(r) {
				var alreadyInList = false;
				
				$("#ResourceList").children().each(function(){
					if(this.getAttribute("title") === r.getAttribute("title") && alreadyInList === false) {
						alreadyInList = true;
					}
				});
				
				if(alreadyInList === false) {
				$("div#ResourcesList").after('<div class="ResourcePreview" style="background-color: aqua; width: 100%; height: 10%; display: table; border-bottom: white 1px solid;" title="' + r.getAttribute('title') + '" onclick="addResourceToCart(this);">	<span class="ResourcePreviewName" style="display: table-cell; text-align: left; vertical-align: middle;">' + r.getAttribute('title') + '</span>  <span class="ResourcePreviewCost" style="display: table-cell; text-align: right; vertical-align: middle;">Cost:' + eval(r.getAttribute('title').toLowerCase()).value +'C/t</span> </div>');
				}
				$(r).remove();
			}
			
			function resetMarket(r) {
				$(r).parent().html(function() {
					return '<div id="CategoriesTag" style="display: table; position: relative; width: 100%; height: 10%; background: red; border-bottom: 1px white solid;"><span class="CategoryName" title="Minerals">Categories</span></div><div class="ResourceCategory"><span class="CategoryName" title="Minerals" onclick="getCategory();">Minerals</span></div><div class="ResourceCategory"><span class="CategoryName" title="Weapons">Weapons</span></div><div class="ResourceCategory"><span class="CategoryName" title="Industry">Industry</span></div><div class="ResourceCategory"><span class="CategoryName" title="Misc">Misc</span></div>';
				});
				$("#CategoriesTag").textfill({});
				$(".ResourceCategory").textfill({});
			}
			
			// Effectuates a transaction on the market
			function completeTransaction() {
				var cost = 0;
				
				$("#Resources").find('input').each(function(){
					if($(this).attr('type') === "number") {
						cost += parseInt(this.value) * eval($(this).parent().parent().attr('title').toLowerCase()).value;
					}else {
						alert("Not a number!");
					}
				});
				
					$("#Resources").find('input').each(function() {
						log(parseInt(this.value) <= cargo[$(this).parent().parent().attr('title')] && cargo[$(this).parent().parent().attr('title')] > 0);
						if(cost <= credits && 0 - parseInt(this.value) <= cargo[$(this).parent().parent().attr('title')] && cargo[$(this).parent().parent().attr('title')] > 0 && this.value < 0) {
							credits -= parseInt(this.value) * (eval($(this).parent().parent().attr('title').toLowerCase()).value / 2);
							cargo[$(this).parent().parent().attr('title')] += parseInt(this.value);
						}else if(cost <= credits && this.value > 0) {
							credits -= parseInt(this.value) * eval($(this).parent().parent().attr('title').toLowerCase()).value;
							cargo[$(this).parent().parent().attr('title')] += parseInt(this.value);
						}else{
							alert("You can't get 0 of something!");
						}
					});
	
			}
			
			//Starts mining
			function startMining() {
				
			}
			
			//Mines a resource from an asteroid
			function mine() {
				var mineralID = (Math.floor(Math.random() * 175) + 1);
				if(mineralID > 0 && mineralID <= 5) {
					cargo["Uranium"]++;
				}else if(mineralID >= 6 && mineralID <= 12) {
					cargo["Platinum"]++;
				}else if(mineralID >= 13 && mineralID <= 21) {
					cargo["Titanium"]++;
				}else if(mineralID >= 22 && mineralID <= 37) {
					cargo["Copper"]++;
				}else if(mineralID >= 38 && mineralID <= 55) {
					cargo["Zinc"]++;
				}else if(mineralID >= 56 && mineralID <= 75) {
					cargo["Iron"]++;
				}else if(mineralID >= 76 && mineralID <= 97) {
					cargo["Chromium"]++;
				}else if(mineralID >= 98 && mineralID <= 121) {
					cargo["Aluminium"]++;
				}else if(mineralID >= 122 && mineralID <= 147) {
					cargo["Silicon"]++;
				}else if(mineralID >= 148 && mineralID <= 175) {
					cargo["Magnesium"]++;
				}
			}
			
			//Start a crafting process in a station
			function startCrafting(blueprint, station) {
				var currentResources = 0;
				var inputLength = blueprint.input.length;
				var enoughResources = false;
				
				for(var i = 0; i < inputLength; i++){
					if(cargo[blueprint.input[i][0].name] >= blueprint.input[i][1]) {
						currentResources++;
					}
				}
				
				if(currentResources >= inputLength) {
					enoughResources = true;
				}else{
					log("Not enough resources!");
					log("C: " + currentResources + " N: " + inputLength);
				}
				
				if(enoughResources && industryTasksInProgress.length === 0) {
					for(var i = 0; i < inputLength; i++) {
						cargo[blueprint.input[i][0].name] -= blueprint.input[i][1];
					}
					var t1 = new IndustryTask(blueprint, null);
					industryTasksInProgress.push(t1)
					game.time.events.add(Phaser.Timer.SECOND * blueprint.craftingTime, endCrafting, this);
				}else if(enoughResources && industryTasksInProgress.length > 0) {
					for(var i = 0; i < inputLength; i++) {
						cargo[industryTasksInProgress[industryTasksInProgress.length - 1].blueprint.input[i][0].name] -= industryTasksInProgress[industryTasksInProgress.length - 1].blueprint.input[i][1];
					}
					
					var t2 = new IndustryTask(blueprint, null);
					industryTasksInProgress.push(t2);
				}
			}
			
			function startCraftingTasks(blueprint) {
				for(var i = 0, j = parseInt($("#BlueprintGeneralInfoAmountField").val()); i < j; i++) {
					startCrafting(blueprint, null);	
				}
			}
			
			//Ends the crafting process, giving the player the objects he was crafting
			function endCrafting() {
				cargo[industryTasksInProgress[0].blueprint.output.name]++;
				log("Crafting task " + industryTasksInProgress[0].name + " completed.");
				
				//Removes the task from the task list
				industryTasksInProgress.splice(0, 1);
				
				if(industryTasksInProgress.length > 0) {
					
					game.time.events.add(Phaser.Timer.SECOND * industryTasksInProgress[0].blueprint.craftingTime, endCrafting, this);
				}
			}
			
			//Updates the blueprint list so that it contains all of the blueprints you currently have
			function updateBlueprintList() {
				$("#BlueprintsList").html(function() {
					return '<div id="BlueprintSearchList"><span id="BlueprintSearchBarSpan"><input type="text" id="BlueprintSearchBar" /></span></div>';
				});
				
				for(var i = 0; i < blueprints.length; i++) {
					$("#BlueprintSearchList").after(function() {
						return '<div class="Blueprint" onclick="loadBlueprintInputList(eval(' + blueprints[i].name +'))"><span class="BlueprintOutputName">' + blueprints[i].output.name + '</span><span class="BlueprintTimeRequired">' + blueprints[i].craftingTime + '</span></div>';
					});
				}
			}
			
			function loadBlueprintInputList(blueprint) {
				var requiredResources = blueprint.input.length;
				
					$("#BlueprintInputList").html(function() {
						return '<div id="BlueprintInputLabel"><span id="BlueprintInputLabelText">Required Materials</span></div>';	
					});
					
					$("#BlueprintGeneralInfo").html(function() {
						return '<div id="BlueprintGeneralInfoLabel"><span id="BlueprintGeneralInfoLabelText">General Information</span></div><div id="BlueprintGeneralInfoInfo"><span id="BlueprintGeneralInfoOutput">Output : ' + blueprint.output.name + '</span></br><span id="BlueprintGeneralInfoCraftingTime">Crafting Time : ' + blueprint.craftingTime + ' </span></div><div id="BlueprintGeneralInfoInput"><div id="BlueprintGeneralInfoAmountFieldContainer"><input id="BlueprintGeneralInfoAmountField" type="number" /></div><div id="BlueprintStartProcessing"><div id="BlueprintStartButton" onclick="startCraftingTasks(eval(' + blueprint.name + '))">Start Processing</div></div></div></div>';
					});
				
				for(var i = 0; i < requiredResources; i++) {
					log(blueprint.input[i][0].name);
					
					$("#BlueprintInputLabel").after(function() {
						if(cargo[blueprint.input[i][0].name] >= blueprint.input[i][1]) {
							return '<div class="BlueprintInput"><span class="BlueprintInputName">' + blueprint.input[i][0].name +'</span><span class="BlueprintQuantityRequired" style="color: green;">' + blueprint.input[i][1] +'</span></div>';
						}else{
							return '<div class="BlueprintInput"><span class="BlueprintInputName">' + blueprint.input[i][0].name +'</span><span class="BlueprintQuantityRequired" style="color: red;">' + blueprint.input[i][1] +'</span></div>';
						}
					});
				}
			}
			
			//Saving and loading the game
			function save(saveName) {
				
			}
			
			function load(saveName) {
				
			}