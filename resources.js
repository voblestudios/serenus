//Main variables
var minerals;
var weapons;
var industry;
var misc;

//Create the Resource object
function Resource(name, value, base, demand, solid) {
	this.name = name;
	this.value = value;
	this.base = base;
	this.demand = demand;
	this.solid = solid;
}

//Solid resources
var magnesium = new Resource("Magnesium", 1, 1, 0, true);
var silicon = new Resource("Silicon", 5, 5, 0, true);
var aluminium = new Resource("Aluminium", 10, 10, 0, true);
var chromium = new Resource("Chromium", 15, 15, 0, true);
var iron = new Resource("Iron", 25, 25, 0, true); 		
var zinc = new Resource("Zinc", 35, 35, 0, true); 		
var copper = new Resource("Copper", 45, 45, 0, true);	
var titanium = new Resource("Titanium", 50, 50, 0, true);
var platinum = new Resource("Platinum", 70, 70, 0, true);
var uranium = new Resource("Uranium", 100, 100, 0, true);

//Change the demand for a resource in the market
function changeDemand(amount, name) {
	if(name.demand > 0) {
		name.demand += amount;
	}
}

//Set the demand for a resource in the market
function setDemand(value, name) {
	if(value > 0) {
		name.demand = value;
	}else{
		alert("Oops! There seems to be a bug...");
	}
}

minerals = [magnesium, silicon, aluminium, chromium, iron, zinc, copper, titanium, platinum, uranium];